﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SablonaNakup
{

    interface IGeneratorMapy
    {

        char[,] Generuj(int sirka, int vyska, int pocetZdi, int pocetNabidek);

        void PridejOsoby<T>(List<T> osoby, char[,] mapa) where T : DataOsoba;

        void NovaNabidka(char[,] mapa);
    }

    class NahodnyGeneratorMapy : IGeneratorMapy
    {
        private Random r;

        public NahodnyGeneratorMapy()
        {
            r = new Random();
        }

        /// <summary>
        /// Generuje data mapy náhodně. Kazdy objekt dává na mapu čistě náhodně bez ohledu
        /// na současná data na mapě. Objekty se tak navzájem můžou přepisovat a počty
        /// <paramref name="pocetNabidek"/> a <paramref name="pocetZdi"/> mají spíš význam horního odhadu.
        /// </summary>
        /// <param name="sirka">Šířka výsledné mapy (první rozměr 2d pole).</param>
        /// <param name="vyska">Výška výsledné mapy (druhý rozměr 2d pole).</param>
        /// <param name="pocetZdi">Počet zdí, které budou na mapě.</param>
        /// <param name="pocetNabidek">Počet nabídek, které budou na mapě.</param>
        /// <returns></returns>
        public char[,] Generuj(int sirka, int vyska, int pocetZdi, int pocetNabidek)
        {
            char[,] mapa = new char[sirka, vyska];
            for (int i = 0; i < sirka; i++)
            {
                for (int j = 0; j < vyska; j++)
                {
                    mapa[i, j] = ' ';
                }
            }
            for (int i = 0; i < pocetZdi; i++)
            {
                int x = r.Next(mapa.GetLength(0));
                int y = r.Next(mapa.GetLength(1));
                mapa[x, y] = 'X';
            }
            for (int i = 0; i < pocetNabidek; i++)
            {
                int x = r.Next(mapa.GetLength(0));
                int y = r.Next(mapa.GetLength(1));
                int cena = r.Next(1, 10);
                mapa[x, y] = cena.ToString()[0];
            }
            return mapa;
        }

        public void PridejOsoby<T>(List<T> osoby, char[,] mapa) where T:DataOsoba
        {
            foreach (var osoba in osoby)
            {
                osoba.Souradnice = generujVolneSouradnice(mapa);
                mapa[osoba.Souradnice.X, osoba.Souradnice.Y] = osoba.Identifikator;
            }
        }

        public void NovaNabidka(char[,] mapa)
        {
            int cena = r.Next(1, 10);
            Souradnice s = generujVolneSouradnice(mapa);
            mapa[s.X, s.Y] = cena.ToString()[0];
        }

        /// <summary>
        /// Hleda volne souradnice v mape opakovanym zkousenim.
        /// Toto je jednoduche reseni, ale obvykle je to spatny napad. Za prve to porusuje podminku na 
        /// konecnost algoritmu. Pokud mam smulu, nemusi to teoreticky skoncit.
        /// 
        /// V pripade plne mapy to dokonce neskonci jiste. V pripade temer plne mapy ta funkce bude realne
        /// trvat velmi dlouho.
        /// 
        /// Tady si tim setrim praci. Pocitam s tim, ze mapa je ridka, takze by to melo dobehnout brzy.
        /// </summary>
        /// <param name="mapa">Mapa, ve ktere hledam volne souradnice.</param>
        /// <returns>Nahodne vygenerovana souradnice, na ktere je v <paramref name="mapa"/> mezera.</returns>
        private Souradnice generujVolneSouradnice(char[,] mapa)
        {
            Souradnice s;
            s.X = r.Next(mapa.GetLength(0));
            s.Y = r.Next(mapa.GetLength(1));
            while (mapa[s.X, s.Y] != ' ')
            {
                s.X = r.Next(mapa.GetLength(0));
                s.Y = r.Next(mapa.GetLength(1));
            }
            return s;
        }
    }
}
